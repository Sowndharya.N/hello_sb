package com.example.hello.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class demo {
   //@GetMapping()
   //public String hello () {
   //return "Hello Abdul";
   //}
   
   @GetMapping("/{name}/{id}")
   //@ResponseBody
   public String name(@PathVariable String name, @PathVariable int id){
	return name +id;
	
   }
   
   @PostMapping("/{ie}")
   //@ResponseBody
   public int ie(@RequestParam("ie") int ie) {	
	return ie;
	   
   }
   
   @GetMapping("/{na}")
   //@ResponseBody
   public String name(@RequestParam("na") String na) {
   return na;
}
   

}
